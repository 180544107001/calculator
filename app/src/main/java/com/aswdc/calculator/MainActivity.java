package com.aswdc.calculator;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;


public class MainActivity extends Activity {

    Button btnClr , btn0 , btn1 , btn2 , btn3 , btn4, btn5 , btn6 , btn7 , btn8 , btn9 , btnAdd , btnSub , btnMul , btnDiv , btnEq;
    TextView textView1 ;
    double i1 , i2 , result=0;
    boolean newValue = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView1 = (TextView) findViewById(R.id.textView1);

        btnClr = (Button) findViewById(R.id.btnClr);
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSub = (Button) findViewById(R.id.btnSub);
        btnMul = (Button) findViewById(R.id.btnMul);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        btnEq = (Button) findViewById(R.id.btnEq);

        btn0.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                textView1.append("0");
            }
        });

        btn1.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("1");
                else
                    textView1.setText(textView1.getText().toString() +  "1");

                newValue = false;
            }
        });

        btn2.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("2");
                else
                    textView1.setText(textView1.getText().toString() +  "2");

                newValue = false;
            }
        });

        btn3.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("3");
                else
                    textView1.setText(textView1.getText().toString() +  "3");

                newValue = false;
            }
        });

        btn4.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("4");
                else
                    textView1.setText(textView1.getText().toString() +  "4");

                newValue = false;
            }
        });

        btn5.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("5");
                else
                    textView1.setText(textView1.getText().toString() +  "5");

                newValue = false;
            }
        });

        btn6.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("6");
                else
                    textView1.setText(textView1.getText().toString() +  "6");

                newValue = false;
            }
        });

        btn7.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("7");
                else
                    textView1.setText(textView1.getText().toString() +  "7");

                newValue = false;
            }
        });

        btn8.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("8");
                else
                    textView1.setText(textView1.getText().toString() +  "8");

                newValue = false;
            }
        });

        btn9.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                if(newValue == true)
                    textView1.setText("9");
                else
                    textView1.setText(textView1.getText().toString() +  "9");

                newValue = false;
            }
        });


        btnAdd.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                i1 = Double.parseDouble(textView1.getText().toString());
                textView1.setText("");
                result = result + i1;
                textView1.setText(Double.toString(result));
                newValue=true ;


            }
        });

        btnSub.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                i1 = Double.parseDouble(textView1.getText().toString());
                result = result - i1;
                textView1.setText(Double.toString(result));
                newValue=true ;
                textView1.setText("");
            }
        });

        btnMul.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                i1 = Double.parseDouble(textView1.getText().toString());
                result = result * i1;
                textView1.setText(Double.toString(result));
                newValue=true ;
                textView1.setText("");
            }
        });

        btnDiv.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                i1 = Double.parseDouble(textView1.getText().toString());
                result = result / i1;
                textView1.setText(Double.toString(result));
                newValue=true ;
                textView1.setText("");

            }
        });

        btnEq.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                textView1.setText(Double.toString(result));
            }
        });

        btnClr.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {

                textView1.setText("");
                newValue=true;
                result = 0 ;
                i1=0;
            }
        });

    }


}
